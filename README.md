# unity-unpacker

unity-unpacker is a tiny, simplistic CLI utility for unpacking the assets from Unity's .unitypackage format.

# installation

The easiest way to install unity-unpacker is through cargo:

```
cargo install unity-unpacker
```

# usage

To unpack a file, you need to specify the source file and destination directory (unity-unpacker will create the destination directory, if it does not already exist):

```
unity-unpacker source-package.unitypackage destination-dir
```

# troubleshooting

If files appear to be missing from the unpacked directory, you can troubleshoot by specifying verbose mode, to see a list of all unpacked files, as well as explanations for any skipped file:

```
unity-unpacker source destination -v
```

unity-unpacker works by unpacking the .unitypackage to a temporary directory, then moving individual asset files into a new directory based on specified pathname information.

You can troubleshoot by telling unity-unpacker not to delete the temporary directory after mapping:

```
  unity-unpacker source destination --no-clean
```

You can also skip the mapping phase entirely to get a look at the raw untouched files:

```
unity-packer source destination --no-clean --no-map
```

