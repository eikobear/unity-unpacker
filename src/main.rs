use flate2::read::GzDecoder;
use std::{fs, path::PathBuf};
use tar::Archive;

const TMP_DIR: &str = "unity-unpacker-tmp-dir";

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let noarg = String::new();
    let input = args.get(1).unwrap_or(&noarg);
    let output = args.get(2).unwrap_or(&noarg);

    if input == "" {
        println!("error: no input file specified!");
        return;
    }

    if output == "" {
        println!("error: no output destination specified!");
        return;
    }

    let verbose = args.iter().any(|arg| arg == "-v");
    let do_clean = !args.iter().any(|arg| arg == "--no-clean");
    let do_map = !args.iter().any(|arg| arg == "--no-map");

    let maindir = PathBuf::from(output);
    let package = fs::File::open(input).unwrap();
    let mut archive = Archive::new(GzDecoder::new(package));

    println!("unpacking {}...", input);
    archive.unpack(TMP_DIR).unwrap();

    let dirs = fs::read_dir(TMP_DIR).unwrap();

    if do_map {
        println!("mapping assets...");
        for dir in dirs {
            let dir_path = dir.unwrap().path();
            let asset_path = dir_path.join("asset");
            let path_path = dir_path.join("pathname");
            let pathdata = fs::read_to_string(path_path);
            if let Err(err) = pathdata {
                if verbose {
                    println!(
                        "  error finding path data at {} ({})",
                        dir_path.display(),
                        err
                    );
                }
                // ignore anything that isn't formatted like an asset
                continue;
            }
            let pathname = maindir.join(pathdata.unwrap().lines().nth(0).unwrap());

            if !asset_path.exists() {
                // if path exists but asset doesn't, it's a directory
                fs::create_dir_all(&pathname).unwrap();
                if verbose {
                    println!("  created dir: {}", pathname.display());
                }
                continue;
            }

            let prefix = pathname.parent().unwrap();
            fs::create_dir_all(prefix).unwrap();
            let result = fs::rename(&asset_path, &pathname);

            if let Err(err) = result {
                if verbose {
                    println!(
                        "  error mapping {} -> {} ({})",
                        asset_path.display(),
                        pathname.display(),
                        err
                    );
                }
            } else if verbose {
                println!("  {} -> {}", asset_path.display(), pathname.display());
            }
        }
    }

    // clean up temp files!
    if do_clean {
        println!("cleaning temporary files...");
        fs::remove_dir_all(TMP_DIR).unwrap();
    }

    println!("unpacked to {}", output);
}
